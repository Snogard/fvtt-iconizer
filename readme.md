# Iconizer
A simple Foundry VTT module that sets icon automatically

## Requiements

without this module enabled iconizer will not work
https://gitlab.com/foundry-azzurite/settings-extender

## Definitions

Shared Internal Storage: a folder shared between worlds stored on the server


## How it works


Example Folder structure

>icons  
>|-default
>  |-default-spell.png
>  |-default-class.png
>|-uncategorized
>  |-spoon.jpg
>|-Feats
>  |-allertness.svg
>  |-power-attack.png
>|-Spells
>  |-fireball.png
>  |-magic-missle.png
>...

When you create or update an item the module looks for a valid icon from your local storage
> assets/icons/{itemType}/{itemName}.png  

Spaces and "_" in {itemName} are replaced with "-" when searching for a valid icon


## Features

Done: 
- support for png files 
- valid icon search inside a shared internal storage
- valid icon search inside a world internal storage 
- support for "default" icons 
- support for "uncategorized" icons 
- default icon settings


Todo: 
- support all image file supported by foundry
- a button to update all icons at once
- support for default icon linked to names
- support for extension search priority