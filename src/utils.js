export let utils = {
    serverFileExists(path) {
        let http = new XMLHttpRequest();
        http.open("HEAD", path, false);
        http.send();
        return http.status < 400;
    },

    log(type, text) {
        console.log("iconizer | ".concat(type, ": ", text));
    },

    isGetter(obj, prop) {
        return !!Object.getOwnPropertyDescriptor(obj, prop)['get'];
    }
};