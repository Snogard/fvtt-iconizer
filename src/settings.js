import "./costants.js";
import { hookType, scopeType } from "./foundry.js";
import { iconizerSettings } from "./costants.js";

Hooks.once(hookType.module.init, registerSettings);

function registerSettings() {
    // game.settings.register(modSettingName, "cloud-shared-repo",
    //     {
    //         name: "Shared Cloud Storage",
    //         hint: "tbd",
    //         default: "",
    //         type: String,
    //         scope: "world",
    //         config: true,
    //     });

    game.settings.register(iconizerSettings.name, iconizerSettings.repos.internalSharedRepo, {
        name: "Shared Internal Storage",
        hint: "Use this folder to save icons shared between worlds",
        default: "assets/icons",
        type: String, //TODO firectory picker
        scope: scopeType.world,
        config: true,
    });

    // game.settings.register(modSettingName, "cloud-world-repo",
    //     {
    //         name: "World Cloud Storage",
    //         hint: "tbd",
    //         default: "",
    //         type: String,
    //         scope: "world",
    //         config: true,
    //     });

    let worldDefault = "worlds/".concat(game.data.world.name, "/data/icons");

    game.settings.register(iconizerSettings.name, iconizerSettings.repos.internalWorldRepo, {
        name: "World Internal Storage",
        hint: "Use this folder to define world specific icons",
        default: "worlds/".concat(game.data.world.name, "/data/icons"),
        type: String,
        scope: scopeType.world,
        config: true,
    });

    game.settings.register(iconizerSettings.name, iconizerSettings.other.defaults, {
        name: "Icons considered to be defaults",
        hint: "add comma separated icon paths",
        default: "default-;icons/svg/mystery-man.svg",
        type: String,
        scope: scopeType.world,
        config: true,
    });
}