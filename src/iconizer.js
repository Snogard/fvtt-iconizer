import "./utils.js";
import { utils } from "./utils.js";
import { hookType } from "./foundry.js";
import { iconizerSettings } from "./costants.js";

// Hooks 
Hooks.on(hookType.item.preUpdate, (entity, updates) => {
    if (updates.name || updates.img) {
        processItemHook(updates, entity);
    }
});

Hooks.on(hookType.item.preCreate, (entity, updates) => {
    processItemHook(updates, entity);
});

// globals

const imgExtensions = [".png", ".svg", ".jpg", ".webp"];

// Functions
function getDefaultDenomination(name) { return "default-".concat(name) };

function getValidName(name) { return name.toLowerCase().replace(" ", "-").replace(",", "").replace("_", "").replace("'", "").replace("\"", ""); };

function processItemHook(updates, entity) {

    let itemData = {};
    itemData.name = updates.name ? updates.name : entity.name;
    itemData.iconPath = updates.img ? updates.img : (entity.data ? entity.img : null);
    itemData.type = updates.type ? updates.type : entity.type;

    let iconPath = replaceIcon(itemData);

    updates.img = iconPath;
    if (entity.img == null) entity.img = iconPath;
    utils.log("event", updates.img);
}

function replaceIcon(itemData) {

    //utils.log(options);
    if (!itemData) return itemData;

    let isDefaultIcon = false;

    let defaultIcons = game.settings.get(iconizerSettings.name, iconizerSettings.other.defaults);
    let defaultArr = defaultIcons.split(";");

    if (itemData.iconPath) {
        for (let i = 0; i < defaultArr.length; i++) {
            if (itemData.iconPath.toLowerCase().indexOf(defaultArr[i]) !== -1) {
                isDefaultIcon = true;
                break;
            }
        }
    }

    if (isDefaultIcon ||
        !utils.serverFileExists(itemData.iconPath)) {
        let newIconPath = findIcon(itemData);
        if (newIconPath != null) {
            return newIconPath;
        }
    }

    return itemData.iconPath;
}

function findIcon(itemData) {
    utils.log("findicon", "start");

    let folder = "";

    //internal world
    folder = game.settings.get(iconizerSettings.name, iconizerSettings.repos.internalWorldRepo);
    let iconPath = checkIcon(itemData, folder, imgExtensions);
    if (iconPath) {
        utils.log("findicon", iconizerSettings.repos.internalWorldRepo);
        return iconPath;
    }

    //internal shared
    folder = game.settings.get(iconizerSettings.name, iconizerSettings.repos.internalSharedRepo);
    iconPath = checkIcon(itemData, folder, imgExtensions);
    if (iconPath) {
        utils.log("findicon", iconizerSettings.repos.internalSharedRepo);
        return iconPath;
    }

    // //cloud world
    // folder = game.settings.get(iconizerSettings.name, iconizerSettings.repos.cloudWorldRepo);
    // iconPath = checkIcon(itemData, folder, imgExtensions);
    // if (iconPath) {
    //     utils.log("findicon", iconizerSettings.repos.cloudWorldRepo);
    //     return iconPath;
    // }

    // //cloud shared
    // folder = game.settings.get(iconizerSettings.name, iconizerSettings.repos.cloudSharedRepo);
    // iconPath = checkIcon(itemData, folder, imgExtensions);
    // if (iconPath) {
    //     utils.log("findicon", iconizerSettings.repos.cloudSharedRepo);
    //     return iconPath;
    // }

    utils.log("findicon", "not found");
    return null;
}

function checkIcon(itemData, folder, extensions) {
    var searchPaths = generateIconSearchPaths(folder, itemData.name, itemData.type);

    for (let p = 0; p < searchPaths.length; p++) {
        var currentPath = searchPaths[p];

        for (let i = 0; i < extensions.length; i++) {

            let iconPath = currentPath.concat(extensions[i]);
            utils.log("checkIcon", "path: ".concat(iconPath));
            let iconExists = utils.serverFileExists(iconPath);

            if (iconExists) {
                utils.log("checkIcon", "found");
                return iconPath;
            }
        }
    }

    utils.log("checkIcon", "not found");
    return null;
}

function generateIconSearchPaths(folder, name, category) {
    let fileName = getValidName(name);

    let paths = {
        categorized: folder.concat("/", category, "/", fileName),
        uncategorized: folder.concat("/uncategorized/", fileName),
        default: folder.concat("/default/", getDefaultDenomination(category)),
    };

    if (category != null) {
        return new Array(paths.categorized, paths.uncategorized, paths.default);
    } else {
        return new Array(paths.uncategorized);
    }
}