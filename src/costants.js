export const iconizerSettings = {
    name: "fvtt-iconizer",
    repos: {
        internalSharedRepo: "internal-shared-repo",
        internalWorldRepo: "internal-world-repo",
        cloudSharedRepo: "cloud-shared-repo",
        cloudWorldRepo: "cloud-world-repo",
    },
    other: {
        defaults: "additional-default-icons"
    }
};