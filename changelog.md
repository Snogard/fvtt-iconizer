# Changelog

## v1.0.0
### Added
- support for svg, jpg, webp files
- icon search inside a world storage
- setting to specify default icons

### Changed
- minimum supported version updated to 0.6.5

---

## v0.1.0

### Added 
- support png files 
- valid icon search inside a shared internal storage